<?php

namespace Zf2TaskManagerExampleTask;

use Zend\Console\Console;
use Zend\EventManager\EventInterface as Event;
use Zend\ModuleManager\Feature\BootstrapListenerInterface;
use Zend\ModuleManager\Feature\ConfigProviderInterface;

class Module implements BootstrapListenerInterface, ConfigProviderInterface
{
    public function getConfig()
    {
        return include __DIR__ . "/config/module.config.php";
    }

    public function onBootstrap(Event $e)
    {
        if (Console::isConsole()) {
            return;
        }
    }
}