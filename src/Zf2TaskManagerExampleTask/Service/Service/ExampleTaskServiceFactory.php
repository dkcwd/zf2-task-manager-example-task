<?php

namespace Zf2TaskManagerExampleTask\Service\Service;

use Zf2TaskManagerExampleTask\Service\ExampleTaskService;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class ExampleTaskServiceFactory implements FactoryInterface
{
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $task = new ExampleTaskService();
        return $task;
    }
}
