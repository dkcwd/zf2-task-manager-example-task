<?php

namespace Zf2TaskManagerExampleTask\Service;

use Zf2TaskManager\Event\TaskEvent;
use Zend\EventManager\EventManagerAwareInterface;
use Zend\EventManager\EventManagerAwareTrait;

/**
 * Note that this service implements EventManagerAwareInterface, when the service is instantiated
 * using the service manager then the event manager is injected into the service automatically.
 *
 * This is a key part of the approach used by Zf2TaskManager so make sure you do it or manually inject
 * the event manager instance to which the events have been attached ('registered') to.
 *
 * Class ExampleTaskService
 * @package Zf2TaskManagerExampleTask\Service
 */
class ExampleTaskService implements EventManagerAwareInterface
{
    use EventManagerAwareTrait;

    public function doSomethingWhichResultsInTriggeringAnEvent()
    {
        // Do whatever you need to do to get the values you want to pass into a TaskEvent
        $someValueWePretendWeGotFromSomeOtherMethodCall = time();

        // Define the TaskEvent
        $event = new TaskEvent(
            // a friendly name
            'some-example-task',
            // a simple way of ensuring it can be triggered
            '*',
            // An array of parameters to make available in the task as content
            array(
                'an_identifier_we_use_to_retrieve_the_value_in_the_task' => $someValueWePretendWeGotFromSomeOtherMethodCall
            )
        );

        // Trigger the event
        return $this->getEventManager()->trigger($event);
    }
}
