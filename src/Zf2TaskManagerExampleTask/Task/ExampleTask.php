<?php

namespace Zf2TaskManagerExampleTask\Task;

use SlmQueue\Job\AbstractJob;

class ExampleTask extends AbstractJob
{
    /**
     * Execute the job
     */
    public function execute()
    {
        /**
         * Do the thing, could be sending an email or an expensive workflow involving 3rd party API calls
         * In this case we will update a view script with the date and time when the task was executed
         *
         * see "dkcwd/zf2-task-manager-callback" for simple callbacks to a url or alternative method
         * see "dkcwd/zf2-task-manager-email-notification" for easy HTML emails
         */

        $content = $this->getContent();
        $timePassedIn = $content['an_identifier_we_use_to_retrieve_the_value_in_the_task'];
        $message = PHP_EOL . 'time passed in was ' . $timePassedIn . ' executed at ' . time();

        file_put_contents(
            __DIR__ . '/../../../view/zf2-task-manager-example-task/i-will-be-updated.phtml',
            $message,
            FILE_APPEND
        );

    }
}
