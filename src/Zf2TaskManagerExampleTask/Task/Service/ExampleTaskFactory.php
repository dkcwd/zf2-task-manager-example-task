<?php

namespace Zf2TaskManagerExampleTask\Task\Service;

use SlmQueue\Job\AbstractJob;
use Zf2TaskManagerExampleTask\Task\ExampleTask;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class ExampleTaskFactory implements FactoryInterface
{
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $task = new ExampleTask();
        return $task;
    }
}
