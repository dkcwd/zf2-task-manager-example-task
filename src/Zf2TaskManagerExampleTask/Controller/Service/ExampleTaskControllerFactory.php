<?php

namespace Zf2TaskManagerExampleTask\Controller\Service;

use Zf2TaskManagerExampleTask\Controller\ExampleTaskController;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class ExampleTaskControllerFactory implements FactoryInterface
{
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        // Instantiate controller and inject the example task service
        $controller = new ExampleTaskController();
        $controller->setExampleTaskService(
            $serviceLocator->getServiceLocator()->get(
                'Zf2TaskManagerExampleTask\Service\ExampleTaskService'
            )
        );

        return $controller;
    }
}
