<?php

namespace Zf2TaskManagerExampleTask\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

class ExampleTaskController extends AbstractActionController
{
    protected $exampleTaskService;

    /**
     * @param mixed $exampleTaskService
     */
    public function setExampleTaskService($exampleTaskService)
    {
        $this->exampleTaskService = $exampleTaskService;
    }

    /**
     * @return mixed
     */
    public function getExampleTaskService()
    {
        return $this->exampleTaskService;
    }

    /**
     * Action responsible for calling the service method which results in a task
     * being queued or executed based on the task manager strategy
     *
     * @return ViewModel
     */
    public function exampleAction()
    {
        $this->getExampleTaskService()->doSomethingWhichResultsInTriggeringAnEvent();
        return new ViewModel();
    }

    /**
     * Action responsible for showing a log of example task execution
     *
     * @return ViewModel
     */
    public function reviewExampleAction()
    {
        return new ViewModel();
    }
}
