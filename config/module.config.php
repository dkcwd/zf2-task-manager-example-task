<?php

/**
 * Note that dkcwd/zf2-task-manager uses SlmQueue.
 *
 * dkcwd/zf2-task-manager tasks are ultimately based on the AbstractJob class allowing
 * for easy queuing and execution.
 *
 * Redefine the following arrays in your local config, note the recommended approach
 * is that you use a factory class to instantiate the notification task with relevant
 * dependencies
 *
 * For this example module the config registers an example task which is built using a factory.
 *
 */
return array(
    /**
     * Task manager config
     * Registering tasks and confirming the strategy (Enqueue or Execute)
     */
    'task_manager' => array(
        // each notification task should be registered as a friendly name and class name pair
        'registered_tasks' => array(
            // 'friendly_name_for_notification' => 'Fully\Qualified\Class\Name\Of\Notification\Task',
            'some-example-task' => 'Zf2TaskManagerExampleTask\Task\ExampleTask'
        ),
        'strategy' => \Zf2TaskManager\Listener\Service\TaskListenerFactory::EXECUTE,
    ),
    /**
     * SlmQueue config
     * Confirming how the tasks will be built
     */
    'slm_queue' => array(
        'job_manager' => array(
            'factories' => array(
                // 'Fully\Qualified\Class\Name\Of\Notification\Task' => 'Fully\Qualified\Class\Name\Of\Notification\Task\Factory',
                'Zf2TaskManagerExampleTask\Task\ExampleTask' => 'Zf2TaskManagerExampleTask\Task\Service\ExampleTaskFactory'
            ),
        ),
    ),
    /**
     * Controllers
     */
    'controllers' => array(
        'factories' => array(
            'Zf2TaskManagerExampleTask\Controller\ExampleTaskController' => 'Zf2TaskManagerExampleTask\Controller\Service\ExampleTaskControllerFactory',
        ),
    ),
    /**
     * Services
     */
    'service_manager' => array(
        'factories' => array(
            'Zf2TaskManagerExampleTask\Service\ExampleTaskService' => 'Zf2TaskManagerExampleTask\Service\Service\ExampleTaskServiceFactory',
        ),
    ),
    /**
     * View scripts
     */
    'view_manager' => array(
        'template_map' => array(
            'zf2-task-manager-example-task/example-task/example' => __DIR__ . '/../view/zf2-task-manager-example-task/example.phtml',
            'zf2-task-manager-example-task/example-task/review-example' => __DIR__ . '/../view/zf2-task-manager-example-task/i-will-be-updated.phtml',
        ),
    ),
    /**
     * Routes
     */
    'router' => array(
        'routes' => array(
            'zf2-task-manager-example-task-example' => array(
                'type' => 'literal',
                'options' => array(
                    'route'    => '/zf2-task-manager-example-task/example',
                    'defaults' => array(
                        'controller' => 'Zf2TaskManagerExampleTask\Controller\ExampleTaskController',
                        'action'     => 'example',
                    ),
                ),
            ),
            'zf2-task-manager-example-task-review-example' => array(
                'type' => 'literal',
                'options' => array(
                    'route'    => '/zf2-task-manager-example-task/review-example',
                    'defaults' => array(
                        'controller' => 'Zf2TaskManagerExampleTask\Controller\ExampleTaskController',
                        'action'     => 'review-example',
                    ),
                ),
            ),
        ),
    ),
);